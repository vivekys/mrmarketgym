from setuptools import setup, find_packages

setup(name='mrmarketgym',
      version='0.1',
      description='Mr Market Gym is an Open AI Gym environment for stock market.',
      url='Mr Market Gym is an Open AI Gym environment for stock market.',
      author='Vivek Y S',
      author_email='vivek.ys@gmail.com',
      license='Private - Not Distributable and cannot be copied with permission from the author',
      packages=find_packages(exclude=["*example*", "*data*"]),
      zip_safe=False)
