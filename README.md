# README #

Mr Market Gym is an Open AI Gym environment for stock market. It enables to train a reinforcement learning algorithm for
portfolio allocation problem setting. The environment uses backtrader for performing the backtesting.
   