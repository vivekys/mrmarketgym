#!/usr/bin/env python

import numpy as np
from gym import Space

class DateTimeSpace(Space):
    """Defines the observation and action spaces, so you can write generic
    code that applies to any Env. For example, you can choose a random
    action.
    """
    def __init__(self, start, end):
        self.start = np.datetime64(start)
        if(type(end) == str):
            self.end = np.datetime64(end)
            self.delta = self.end - self.start
        else:
            self.delta = np.timedelta64(end, 'D')
            self.end = self.start + self.delta

        self.dtype = np.dtype(self.start)

    def sample(self):
        """
        Uniformly randomly sample a random element of this space
        """
        """
        Returns an array of random dates in the interval [start, end]. Valid 
        resolution arguments are numpy date/time units, as documented at: 
            https://docs.scipy.org/doc/numpy-dev/reference/arrays.datetime.html
        """
        start, end = np.datetime64(self.start), np.datetime64(self.end)
        delta_mat = np.random.randint(0, self.delta.astype('int'), 1)
        samples = start + delta_mat
        return samples[0]

    def seed(self, seed):
        """Set the seed for this space's pseudo-random number generator. """
        self.np_random.seed(seed)

    def contains(self, x):
        """
        Return boolean specifying if x is a valid
        member of this space
        """
        range = np.datetime(x) - self.start
        return range >= 0 and range <= self.delta


    def __contains__(self, x):
        return self.contains(x)
