#!/usr/bin/env python

import logging
import pandas as pd
import backtrader as bt
import multiprocessing
from dateutil.relativedelta import relativedelta
from tensorboardX import SummaryWriter
from mrmarketgym.networking.util import NetworkingUtil
from mrmarketgym.env.btCommission import SimpleCommission
from mrmarketgym.env.DailyLogReturn import DailyLogReturn
from mrmarketgym.env.simulatorStrategy import MarketSimulatorStratergy
from mrmarketgym.env.simulatorDFA import States
from mrmarketgym.env.simulatorDFA import Commands
import gc
import time
import pickle
import os
"""
"""

class MarketSimulator(multiprocessing.Process):
    """
    Creates the Cerebro and adds MarketSimulatorStratergy. Then Cerebro runs once the simulator is started
    """
    def __init__(self,
                 id,
                 queue,
                 start_date,
                 episode_length=1,  #Year
                 max_drawdown=10,  #percent of the portfolio value
                 commision=0.005,
                 start_cash=100,
                 add_cash=100,
                 metrics = False,
                 reporting = False,
                 m_assets=dict(),
                 port=5550,
                 timeout=60):
        super(MarketSimulator, self).__init__()
        self.id = id
        self.start_date = start_date
        self.episode_length = episode_length
        self.max_drawdown = max_drawdown
        self.commision = commision
        self.start_cash = start_cash
        self.add_cash = add_cash
        self.metrics = metrics
        self.reporting = reporting
        self.m_assets = m_assets
        self.port = port
        self.timeout = timeout
        self.writer = None
        self.serverState = States.INITIALIZED
        self.marketSimulatorSocket = queue
        self.resetCount = 1

    def readData(self):
        dfs = []
        for asset_name, path in self.m_assets.items():
            logging.debug(f"Reading File {asset_name} at {path}")
            df = pd.read_csv(path)
            df['Date'] = pd.to_datetime(df['Date'], format='%Y-%m-%d')
            df = df.set_index("Date")
            col = df.columns[0]
            data = df[[col]]
            data = data.rename(columns={col: asset_name})
            dfs.append(data)

        startDate = pd.to_datetime(self.start_date, format='%Y-%m-%d')
        episode = relativedelta(years=self.episode_length)
        one = relativedelta(days=1)
        endDate = startDate + episode - one
        length = (endDate - startDate).days + 1
        finalData = pd.concat(dfs, sort=True, axis=1)
        finalData = finalData.loc[startDate:endDate]
        if(finalData.shape[0] != length):
            logging.warning(f"Actual Data Read is {finalData.shape[0]} is not equal to {length}")

        return finalData

    def createFeeds(self):
        df = self.readData()
        cols = sorted(df.columns)
        btFeeds = []
        for col in cols:
            btFeed = df[[col]]
            btFeed = btFeed.rename(columns={col: 'close'})
            btFeed["close"] = btFeed["close"].interpolate(limit_area='inside')
            btFeed['open'] = btFeed['close']
            btFeed['high'] = btFeed['close']
            btFeed['low'] = btFeed['close']
            btFeed = btFeed.fillna(-1)
            fromdate = btFeed.index[0]
            todate = btFeed.index[-1]
            pdCsv = bt.feeds.PandasData(dataname=btFeed, nocase=True, name=col,
                                        fromdate=fromdate, todate=todate)
            logging.info(f"{col} shape {btFeed.shape} fromdate {fromdate} todate {todate}")
            btFeeds.append((col, pdCsv))
        return btFeeds

    def createAndInitializeCerebro(self):
        if(self.writer != None):
            self.writer.close()
            self.writer = None
        now = int(time.time())
        self.writer = SummaryWriter(log_dir="runs-gym/mrmarketgym-" + str(self.start_date) + "-" + str(now))
        feeds = self.createFeeds()
        cerebro = bt.Cerebro(stdstats=True)
        assetNames = []
        for (name, dataFeed) in feeds:
            cerebro.adddata(data=dataFeed, name=name)
            assetNames.append(name)

        cerebro.broker.setcash(self.start_cash)
        cerebro.broker.set_fundmode(True)
        cerebro.broker.addcommissioninfo(SimpleCommission(commission=self.commision))

        if(self.reporting is True):
            cerebro.addanalyzer(bt.analyzers.PyFolio, _name='PyFolio')

        if(self.metrics is True):
            cerebro.addobserver(bt.observers.DrawDownLength)
            cerebro.addobserver(bt.observers.LogReturns)
            cerebro.addobserver(bt.observers.FundValue)

        cerebro.addobserver(bt.observers.DrawDown)
        cerebro.addobserver(DailyLogReturn)

        cerebro.addstrategy(MarketSimulatorStratergy,
                            writer=self.writer,
                            add_cash=self.add_cash,
                            metrics=self.metrics,
                            max_drawdown=self.max_drawdown,
                            start_date=self.start_date,
                            episode_length=self.episode_length,
                            asset_names=assetNames,
                            marketSimulatorSocket=self.marketSimulatorSocket,
                            serverState = self.serverState)
        logging.info("Created and Initialized Cerebro")
        return cerebro

    def run(self):
        logging.info("Created marketSimulatorSocket")
        logging.info("Starting MarketSimulator")
        cerebro = None
        while(True):
            command = Commands.UNKNOWN
            if(self.serverState == States.INITIALIZED):
                logging.info("MarketSimulator - Waiting to Receive Request")
                request = NetworkingUtil.receive(self.marketSimulatorSocket)
                logging.debug(request)
                command = request["message"].get("command", None)
            else:
                logging.error("Wrong State Transition")

            if(command == Commands.PING):
                message = "PONG"
                status = 0
                NetworkingUtil.send(self.marketSimulatorSocket, {"message": message, "status": status})
            elif(command == Commands.RESET):
                self.start_date = request["message"].get("start_date")
                self.resetCount = self.resetCount - 1
                if(cerebro == None or self.resetCount <= 0):
                    self.resetCount = 1
                    cerebro = self.createAndInitializeCerebro()
                logging.info("Transitioning the Simulator to Running State")
                self.serverState = States.RUNNING
                results = cerebro.run()
                if(self.reporting is True):
                    finalResult = {}
                    strat = results[0]
                    analyzer_res = strat.analyzers.getbyname('PyFolio')
                    items = analyzer_res.get_pf_items()
                    finalResult.update({'PyFolio_items': items})
                    os.makedirs(os.path.join("saves", str(self.start_date)), exist_ok=True)
                    name = str(self.start_date) + ".pkl"
                    fname = os.path.join("saves", str(self.start_date), name)
                    with open(fname, 'wb') as f:
                        pickle.dump(finalResult, f, pickle.HIGHEST_PROTOCOL)
                gc.collect()
                self.serverState = States.INITIALIZED
            elif(command == Commands.CLSOE):
                logging.info("Transitioning the Simulator to Terminated State")
                self.serverState = States.TERMINATED
                message = "CLOSING"
                status = 0
                NetworkingUtil.send(self.marketSimulatorSocket, {"message": message, "status": status})
                break
            else:
                logging.error("Unknown Command Received")
                message = "Unknown Command Received"
                status = -1
                NetworkingUtil.send(self.marketSimulatorSocket, {"message": message, "status": status})
