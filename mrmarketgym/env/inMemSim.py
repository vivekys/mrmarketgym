#!/usr/bin/env python

import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta

class InMemMarketSimulator:
    def __init__(self, startDate, m_assets, episodeLength):
        self.startDate = startDate
        self.m_assets = m_assets
        self.asset_count = len(self.m_assets) + 1
        self.episodeLength = episodeLength

        posVector = self.resetPosVector()
        self.posVector = posVector
        self.currentDate = pd.to_datetime(self.startDate)
        self.currentLength = 0
        self.one = relativedelta(days=1)
        None

    def resetPosVector(self):
        self.posVector = np.zeros(self.asset_count)
        self.posVector[0] = 1

    def step(self, action):
        self.posVector = action
        self.currentDate = self.one + self.currentDate
        return self.getResponse()

    def getResponse(self):
        response = dict()
        response["observations"] = self.getObsertvations()
        response["reward"] = self.getReward()
        response["isDone"] = self.isDone()
        response["info"] = dict()
        return response

    def isDone(self):
        return self.currentLength < self.episodeLength

    def getReward(self):
        return np.random.rand()

    def getObsertvations(self):
        #(date, posVector)
        return (self.currentDate, self.posVector)

    def getInfo(self):
        info = {}
        info.update({"Date": self.currentDate})
        return info

    def reset(self):
        #Reset Date to start date
        #Reset PosVector
        self.resetPosVector()
        self.currentDate = pd.to_datetime(self.startDate)
        self.currentLength = 0
        response = dict()
        response["observations"] = self.getObsertvations()
        return response
        None