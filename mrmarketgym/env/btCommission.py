#!/usr/bin/env python

import backtrader as bt

# Need to change this to reflect exit load for mutual funds
class SimpleCommission(bt.CommInfoBase):
    params = (
        ('stocklike', True),
        ('commtype', bt.CommInfoBase.COMM_PERC)
    )
