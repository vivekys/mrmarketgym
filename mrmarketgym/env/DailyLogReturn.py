#!/usr/bin/env python

import backtrader as bt
import numpy as np

class DailyLogReturn(bt.observers.FundValue):
    lines = ('daily_logret',)

    def next(self):
        super(DailyLogReturn, self).next()
        logFundT = np.log(self.lines.fundval[0])
        logFundTminusOne = np.log(self.lines.fundval[-1])
        change = logFundT - logFundTminusOne
        self.lines.daily_logret[0] = change
