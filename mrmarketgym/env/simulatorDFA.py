#!/usr/bin/env python

"""
Simulator's Deterministic Finite Automate
                PING
Initialized --------------> Initialized
                PONG
                START
Initialized --------------> Started
                OK
            RESET
Started --------------> Running
         Observations
            RESET
Running --------------> Interrupted -----------> Running
         Observations
"""

from enum import Enum

class States:
    INITIALIZED = 1
    RUNNING = 3
    INTERRUPTED = 4
    TERMINATED = 5


class Commands:
    PING = 1
    ACTION = 2
    RESET = 3
    CLSOE = 4
    UNKNOWN = 5

