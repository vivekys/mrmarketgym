#!/usr/bin/env python

import gym
import numpy as np
from gym import spaces
import logging
from mrmarketgym.networking.util import NetworkingUtil
from mrmarketgym.env.simulatorDFA import Commands
from mrmarketgym.env.simulator import MarketSimulator
from mrmarketgym.env.datetimeSpace import DateTimeSpace
import time
import gc
from multiprocessing import Pipe

class MrMarket(gym.Env):
    """
    Description:
        MrMarket is Stock Market Gym environment.
    Observation:
        DateTime at T for which StateManager will return the actual observations
        Portfolio weight vector of size m + 1 with zeroth position reserved for cash and remaining m position for
        weights of m assets.
        Type Tuple(DateTimeSpace(start="SomeDate", end="SomeEndDate"), Box(low=0, high=1, shape=(m+1,)))
    Action:
        Portfolio weight vector of size m + 1 with zeroth position reserved for cash and remaining m position for
        weights of m assets.
        Type: Box(low=0, high=1, shape=(m+1,))
    Reward:
        Reward is basically log(portfolio-fund value at t) - log(portfolio-fund value at t - 1)

    Starting State:
        Observation at time t

    Episode Termination:
        Episode Terminates when the drawdown of the portfolio is greater than some configurable value x
        Or
        Episode Terminates when the length of the episode reaches some configurable value y
    """

    def __init__(self,
                 id,
                 start_date,
                 episode_length = 1, #Year
                 max_drawdown = 10, #percent of the portfolio value
                 commision = 0.005,
                 start_cash = 100,
                 add_cash = 100,
                 assets_cnt = 0,
                 metrics=False,
                 reporting=False,
                 timeout=300,
                 m_assets = dict()):
        self.id = id
        self.start_date = start_date
        self.episode_length = episode_length
        self.max_drawdown = max_drawdown
        self.commision = commision
        self.start_cash = start_cash
        self.add_cash = add_cash
        self.assets_cnt = assets_cnt
        self.metrics = metrics
        self.reporting = reporting
        self.port = None
        self.timeout = timeout
        self.m_assets = m_assets

        self.action_space = spaces.Box(low=0, high=1, dtype=np.float16, shape=(self.assets_cnt, ))
        self.observation_space = spaces.Tuple((DateTimeSpace(start = self.start_date, end = self.episode_length),
                spaces.Box(low=0, high=1, dtype=np.float16, shape=(self.assets_cnt, ))))

        self.simulatorClientSocket = None
        self.__create_MrMarketSimulator()

    def __create_MrMarketSimulator(self):
        self.simulatorClientSocket, self.simulatorServerSocket = Pipe()
        self.marketSimulator = MarketSimulator(
            id = self.id,
            queue = self.simulatorServerSocket,
            start_date = self.start_date,
            episode_length = self.episode_length,
            max_drawdown = self.max_drawdown,
            commision = self.commision,
            start_cash = self.start_cash,
            add_cash = self.add_cash,
            metrics = self.metrics,
            reporting = self.reporting,
            m_assets = self.m_assets,
            port = self.port
        )
        self.marketSimulator.daemon = False
        self.marketSimulator.start()
        time.sleep(10)
        request = dict()
        request["command"] = Commands.PING
        response = NetworkingUtil.send_receive(self.simulatorClientSocket, request)
        if(response["status"] == 0):
            logging.info(f"Market Simulator is Running. Received {response}")
        else:
            logging.error(f"Market Simulator Failed to Start")

    def step(self, action):
        """Run one timestep of the environment's dynamics. When end of
        episode is reached, you are responsible for calling `reset()`
        to reset this environment's state.
        Accepts an action and returns a tuple (observation, reward, done, info).
        Args:
            action (object): an action provided by the environment
        Returns:
            observation (object): agent's observation of the current environment
            reward (float) : amount of reward returned after previous action
            done (boolean): whether the episode has ended, in which case further step() calls will return undefined results
            info (dict): contains auxiliary diagnostic information (helpful for debugging, and sometimes learning)
        """
        request = dict()
        request["command"] = Commands.ACTION
        request["action"] = action
        logging.debug(f"Performing the action in env {self.id} -> {action}")
        response = NetworkingUtil.send_receive(self.simulatorClientSocket, request)
        return (response["message"]["observations"], response["message"]["reward"], \
               response["message"]["isDone"], response["message"]["info"])

    def reset(self, start_date):
        gc.collect()
        """Resets the state of the environment and returns an initial observation.
        Returns: observation (object): the initial observation of the
            space.
        """
        request = dict()
        request["command"] = Commands.RESET
        request["start_date"] = start_date
        logging.info(f"Performing the reset with date {start_date}")
        response = NetworkingUtil.send_receive(self.simulatorClientSocket, request)
        logging.debug(response)
        res = response["message"]
        return res["observations"]

    def render(self, mode='human'):
        """Renders the environment.
        The set of supported modes varies per environment. (And some
        environments do not support rendering at all.) By convention,
        if mode is:
        - human: render to the current display or terminal and
          return nothing. Usually for human consumption.
        - rgb_array: Return an numpy.ndarray with shape (x, y, 3),
          representing RGB values for an x-by-y pixel image, suitable
          for turning into a video.
        - ansi: Return a string (str) or StringIO.StringIO containing a
          terminal-style text representation. The text can include newlines
          and ANSI escape sequences (e.g. for colors).
        Note:
            Make sure that your class's metadata 'render.modes' key includes
              the list of supported modes. It's recommended to call super()
              in implementations to use the functionality of this method.
        Args:
            mode (str): the mode to render with
            close (bool): close all open renderings
        Example:
        class MyEnv(Env):
            metadata = {'render.modes': ['human', 'rgb_array']}
            def render(self, mode='human'):
                if mode == 'rgb_array':
                    return np.array(...) # return RGB frame suitable for video
                elif mode is 'human':
                    ... # pop up a window and render
                else:
                    super(MyEnv, self).render(mode=mode) # just raise an exception
        """
        raise NotImplementedError

    def close(self):
        """Override _close in your subclass to perform any necessary cleanup.
        Environments will automatically close() themselves when
        garbage collected or when the program exits.
        """
        request = dict()
        request["command"] = Commands.CLSOE
        logging.info(f"Closing")
        response = NetworkingUtil.send_receive(self.simulatorClientSocket, request)
        logging.info(f"Closing Command issued. Received Response {response}")
        return response["message"]
