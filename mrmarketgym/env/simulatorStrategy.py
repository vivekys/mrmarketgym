#!/usr/bin/env python

import logging
import backtrader as bt
import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
from mrmarketgym.networking.util import NetworkingUtil
from mrmarketgym.env.simulatorDFA import Commands
from mrmarketgym.env.simulatorDFA import States

class MarketSimulatorStratergy(bt.Strategy):
    params = dict(
        writer=None,
        add_cash=None,
        max_drawdown=None,
        start_date=None,
        episode_length=None,
        asset_names=None,
        marketSimulatorSocket=None,
        serverState=None,
        metrics=False
    )

    def __init__(self):
        self.writer = self.p.writer
        self.add_cash = self.p.add_cash
        self.max_drawdown = self.p.max_drawdown
        self.start_date = self.p.start_date
        self.episode_length_Y = self.p.episode_length
        self.asset_names = sorted(self.p.asset_names)
        self.marketSimulatorSocket = self.p.marketSimulatorSocket
        self.serverState = self.p.serverState,
        self.metrics = self.p.metrics,
        self.current_length = 0
        self.pendingResponse = False
        self.drawdownHit = False

        startDate = pd.to_datetime(self.start_date, format='%Y-%m-%d')
        episode = relativedelta(years=self.episode_length_Y)
        one = relativedelta(days=1)
        endDate = startDate + episode - one
        self.episode_length = (endDate - startDate).days + 1

        if(self.add_cash != None):
            self.add_timer(when=bt.Timer.SESSION_START, monthdays=[15])

    def reset(self):
        self.current_length = 0
        self.drawdownHit = False

    def start(self):
        self.reset()
        response = dict()
        response["observations"] = self.getObsertvations(atT=1)
        NetworkingUtil.send(self.marketSimulatorSocket, response)
        self.pendingResponse = False
        logging.info("Reset Complete")

    def isDone(self):
        if (self.current_length >= self.episode_length):
            logging.info("Episode ended by running all time steps- isDone - True")
            return True
        elif(self.stats.drawdown.maxdrawdown[-1] is not np.nan and
                    self.stats.drawdown.drawdown[-1] > self.max_drawdown):
            logging.info("Episode ended by drawdown hitting max drawdown - isDone - True")
            self.drawdownHit = True
            return True
        else:
            return False

    def getTotalCommission(self):
        trades = self._trades
        commission = 0
        for data, tradeDict in trades.items():
            for key, tradeList in tradeDict.items():
                for trade in tradeList:
                    commission = commission + trade.commission
        return commission

    def updateStats(self):
        commission = self.getTotalCommission()
        cash = self.broker.getcash()
        value = self.broker.getvalue()
        currentDrawdown = self.stats.drawdown.drawdown[-1]
        dailyLogRet = self.stats.dailylogreturn.daily_logret[0] * 100
        fundValue = self.stats.fundvalue.fundval[0]
        step = len(self)
        self.writer.add_scalar("MrMarketGym/cash", cash, step)
        self.writer.add_scalar("MrMarketGym/value", value, step)
        self.writer.add_scalar("MrMarketGym/totalCommission", commission, step)
        self.writer.add_scalar("MrMarketGym/currentDrawdown", currentDrawdown, step)
        self.writer.add_scalar("MrMarketGym/dailyLogRet", dailyLogRet, step)
        self.writer.add_scalar("MrMarketGym/fundValue", fundValue, step)
        positionVector = self.getPositionVector()
        i = 0
        self.writer.add_scalar("MrMarketGym-POS/" + "cash", positionVector[i], step)
        while i < len(self.asset_names):
            name = self.asset_names[i]
            i = i + 1
            self.writer.add_scalar("MrMarketGym-POS/" + name, positionVector[i], step)


    def getPositionVector(self):
        currentPositions = self.getpositionsbyname(self.broker)
        positions = []
        cash_pos = self.broker.getcash()
        positions.append(cash_pos)
        step = len(self) + 1
        sortedPositions = sorted(currentPositions.items())
        for name, position in sortedPositions:
            data = self.getdatabyname(name)
            price = data.close[0] if step > 1 else data.close[1]
            logging.debug(f"{name}  {position.size}  {price}")
            pos = position.size * price
            positions.append(pos)

        arr = np.array(positions)
        total = arr.sum()
        positionVector = arr/total

        return positionVector

    def getObsertvations(self, atT=0):
        info = self.getMetaInfo(atT=atT)
        posVector = self.getPositionVector()
        return (info["Date"], posVector)

    def getMetaInfo(self, atT=0):
        info = {}
        dt = self.data.datetime[atT]
        if isinstance(dt, float):
            dt = bt.num2date(dt)
        info.update({"Date" : dt})
        return info

    def getRewards(self):
        reward = self.stats.dailylogreturn.daily_logret[0] * 100 if (self.drawdownHit is False) else -100.0
        if(np.isnan(reward) == True):
            return 0
        else:
            return reward

    def notify_timer(self, timer, when, *args, **kwargs):
        if self.add_cash is not None:
            self.broker.add_cash(self.add_cash)

    def getResponse(self):
        response = dict()
        response["observations"] = self.getObsertvations()
        response["isDone"] = self.isDone()
        response["reward"] = self.getRewards()
        response["info"] = dict()
        return response

    def next(self):
        self.current_length = self.current_length + 1
        logging.debug(f"Current Time Step {self.current_length}")
        if(self.pendingResponse):
            logging.debug("Sending pendingResponse")
            response = self.getResponse()
            NetworkingUtil.send(self.marketSimulatorSocket, response)
            self.pendingResponse = False
            if(response["isDone"] == True):
                logging.info("Transitioning the Simulator to Initializing State")
                self.serverState = States.INITIALIZED
                if(self.current_length != self.episode_length):
                    self.cerebro.runstop()
                return
        request = NetworkingUtil.receive(self.marketSimulatorSocket)
        logging.debug(request)
        command = request["message"].get("command", None)
        if(command == Commands.ACTION):
            self.serverState = States.RUNNING
            self.pendingResponse = True
            actionVector = request["message"]["action"]
            positionVector = self.getPositionVector()
            logging.debug(f"Position Vector - {positionVector}")
            newPositionVector = positionVector + actionVector
            newPositionVector = np.clip(newPositionVector, a_min=0.0, a_max=1.0)
            total = np.sum(newPositionVector)
            if(total == 0):
                None
            else:
                newPositionVector = newPositionVector / total
                logging.debug(f"New Position Vector - {newPositionVector}")
                adjActionVector = newPositionVector - positionVector
                for i in range(len(adjActionVector[1:])):
                    if(adjActionVector[1:][i] == 0.0):
                        None # No action
                    else:
                        assetName = self.asset_names[i]
                        data = self.getdatabyname(assetName)
                        logging.debug(f"Name - {assetName} Date - {bt.num2date(data.datetime[0])} Close - {data.close[0]}")
                        if(data.close[0] != -1):
                            logging.debug(f"Allocating {assetName}  {newPositionVector[1:][i]}")
                            self.order_target_percent(data, newPositionVector[1:][i])
        elif(command == Commands.RESET):
            self.serverState = States.INTERRUPTED
            self.cerebro.runstop()
        elif(command == Commands.CLSOE):
            self.serverState = States.TERMINATED
            self.cerebro.runstop()
        else:
            logging.error("Unknown command received")

        self.updateStats()

    def notify_timer(self, timer, when, *args, **kwargs):
        if self.add_cash is not None:
            self.broker.add_cash(self.add_cash)