#!/usr/bin/env python

import logging
import time
import pyarrow as pa

class NetworkingUtil:
    @staticmethod
    def send(socket, message):
        response = dict(
            status=0,
            message=None,
        )
        try:
            msg_b = pa.serialize(message).to_buffer().to_pybytes()
            socket.send(msg_b)
        except Exception as e:
            response['status'] = -1
            response['message'] = 'send_failed_for_unknown_reason'
            return response
        return None

    @staticmethod
    def receive(socket):
        response = dict(
            status=0,
            message=None,
        )
        start = time.time()
        try:
            msg_b = socket.recv()
            response['message'] = pa.deserialize(msg_b)
            response['time'] = time.time() - start
        except Exception as e:
            response['message'] = 'receive_failed_for_unknown_reason'
            response['status'] = -1
            return response
        return response

    @staticmethod
    def send_receive(socket, message):
        ret = NetworkingUtil.send(socket, message)
        if(ret == None):
            return NetworkingUtil.receive(socket)
        else:
            return ret
