#!/usr/bin/env python

from mrmarketgym.networking.util import NetworkingUtil
import multiprocessing
import time

class TestServer(multiprocessing.Process):
    def __init__(self, port=5550):
        super(TestServer, self).__init__()

    def run(self):
        self.serverSocket, port_selected = NetworkingUtil.createServerSocket(30)
        print("Creating Server at port : " + str(port_selected))
        while (True):
            print("Waiting")
            request = NetworkingUtil.receive(self.serverSocket)
            print(request)
            NetworkingUtil.send(self.serverSocket, {"message": "PONG"})

if __name__ == '__main__':
    testServer = TestServer()
    testServer.daemon = False
    testServer.start()
    time.sleep(300)
    print("Done Main")