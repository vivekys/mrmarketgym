#!/usr/bin/env python

from mrmarketgym.networking.util import NetworkingUtil
import time

if __name__ == '__main__':
    clientSocket = NetworkingUtil.createClientSocket(5550, 30)
    print(NetworkingUtil.send_receive(clientSocket, {"message" : "PING"}))
