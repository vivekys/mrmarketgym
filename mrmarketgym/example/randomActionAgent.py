#!/usr/bin/env python

import logging
from mrmarketgym.env.mrmarket import MrMarket
import numpy as np
import pandas as pd
import time
import gc
from dateutil.relativedelta import relativedelta


if __name__ == '__main__':
    log_format = '%(asctime)s,%(msecs)d %(levelname)-8s %(filename)s:%(lineno)d %(message)s'
    log_datefmt = '%Y-%m-%d:%H:%M:%S'
    logging.basicConfig(format=log_format, datefmt=log_datefmt, level=logging.DEBUG)
    m_asset = dict()
    m_asset.update({"NIFTY_50": "/Users/vivek/myCode/company/i18r/hygieia/data/01-Feb-2019/cleaned/indexData/NIFTY_50.csv"})
    m_asset.update({"NIFTY_500": "/Users/vivek/myCode/company/i18r/hygieia/data/01-Feb-2019/cleaned/indexData/NIFTY_500.csv"})
    m_asset.update({"NIFTY_GS_10YR": "/Users/vivek/myCode/company/i18r/hygieia/data/01-Feb-2019/cleaned/indexData/NIFTY_GS_10YR.csv"})
    m_asset.update({"NIFTY_BANK": "/Users/vivek/myCode/company/i18r/hygieia/data/01-Feb-2019/cleaned/indexData/NIFTY_BANK.csv"})
    m_asset.update({"NIFTY_IT": "/Users/vivek/myCode/company/i18r/hygieia/data/01-Feb-2019/cleaned/indexData/NIFTY_IT.csv"})
    m_asset.update({"FD": "/Users/vivek/myCode/company/i18r/hygieia/data/01-Feb-2019/cleaned/indexData/FD.csv"})
    episode_length=1
    marketEnv1 = MrMarket(id=1,
                          start_date="2002-04-01",
                          episode_length=episode_length,
                          max_drawdown=10,
                          commision=0.005,
                          start_cash=100000,
                          add_cash=0,
                          assets_cnt=len(m_asset),
                          m_assets=m_asset,
                          metrics=True)


    # marketEnv2 = MrMarket(id=2,
    #                       start_date="2001-04-01",
    #                       episode_length=episode_length,
    #                       max_drawdown=10,
    #                       commision=0.005,
    #                       start_cash=100000,
    #                       add_cash=0,
    #                       assets_cnt=2,
    #                       m_assets=m_asset)

    startDate = pd.to_datetime("2002-04-01", format='%Y-%m-%d')
    end = relativedelta(days=1000)
    endDate = startDate + end
    dates = [d.strftime('%Y-%m-%d') for d in pd.date_range(start=startDate, end=endDate, freq='D')]

    for j in range(1):
        start = time.time()
        currentDate = dates[j]
        response = marketEnv1.reset(currentDate)

        currentDate = pd.to_datetime(currentDate, format='%Y-%m-%d')
        one = relativedelta(days=1)
        episode = relativedelta(years=episode_length)
        endDate = currentDate + episode - one
        days = (endDate - currentDate).days + 1

        # print("After Reset")
        # print("Date - " + str(response[0]))
        # print("PosVector - " + str(response[1]))

        total_reward = 0
        total_steps = 0
        all_rewards = []
        actions = []
        currentPosVector = response[1]
        for i in range(days):
            pVector = np.random.rand(len(m_asset) + 1)
            total = pVector.sum()
            pVector = pVector/total
            changeVector = pVector - currentPosVector
            actions.append(pVector)
            observation, reward, done, info = marketEnv1.step(changeVector)
            currentPosVector = observation[1]
            total_reward = total_reward + reward
            total_steps = total_steps + 1
            all_rewards.append(reward)
            if(done):
                break
        # print("Total Reward for first run = " + str(total_reward))
        # print("Total Steps for first run = " + str(total_steps + 1))
        # print("All Reward for first run:")
        gc.collect()
        end = time.time()
        print(f"{j} Run Time - {end - start} total_steps - {total_steps}")
    # observation = marketEnv1.reset()
    # print("After Reset")
    # print("Date - " + str(response[0]))
    # print("PosVector - " + str(response[1]))
    #
    # print("Second Run")
    # total_reward = 0
    # total_steps = 0
    # all_rewards = []
    # for i in range(episode_length):
    #     pVector = np.random.rand(3,)
    #     total = pVector.sum()
    #     pVector = pVector/total
    #     observation, reward, done, info = marketEnv1.step(actions[i])
    #     total_reward = total_reward + reward
    #     total_steps = i
    #     all_rewards.append(reward)
    #     if(done):
    #         break
    #
    # print("Total Reward for second run = " + str(total_reward))
    # print("Total Steps for second run = " + str(total_steps + 1))
    # print("All Reward for second run:")
    # print(all_rewards)
    #
    # time.sleep(5)
    # print("Closing")
    marketEnv1.close()
#    marketEnv2.close()
    time.sleep(5)
    print("DONE")